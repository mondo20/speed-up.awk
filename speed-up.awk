#!/usr/bin/awk -f
# speed-up.awk
#
# speed up a video while not also speeding up the audio,
# instead creating an audio track of concatenated audio thumbnails.
# needs 'ffmpeg' (including 'ffprobe') installed.
#
# when you speed up a video by a rate greater than 2 or 3,
# the audio, sped up along with the video, gets mangled
# and becomes unlistenable.
#
# this script by default doesn't speed up the audio, instead
# it creates an audio track with stitched together 'audio thumbnails'
# giving a good sense of what the original audio was even if
# the video is sped up 10 times or more.
#
# when you use the script to slow down a video, the audio is likewise
# not being slowed down, but the 'audio thumbnails' are 'shingled'
# (overlapping).


function print_usage()
{
  print "usage: awk -f speed-up.awk videofile [options]\n"\
  "   or: ./speed-up.awk videofile [options]\n"
  print "Options:\n"\
  "  r=[~0.1 - ~50]          speed up factor, default = 10\n"\
  "                          a speed up factor <1 results in a slow-motion video\n"\
  "  thumb=[seconds]         duration of a single audio thumbnail, default = 1.5 seconds\n"\
  "  tempo=[0.5 - 2]         audio tempo change, default = 1 (no change)\n"\
  "  pitch=[~0.2 - ~10]      audio pitch change, default = 1 (no change)\n"\
  "  reversevideo            reverse video (works only for shorter videos)\n"\
  "  reverseaudio            reverse audio\n"\
  "  dontrun                 don't run the generated script (so it can be edited).\n"\
  "                          you can run the script manually with './speedup.sh'\n"
  print "Examples:\n"\
  "  speed up a movie 15 times with 1 sec. audio thumbnails, speed the audio up 1.5 times:\n\n"\
  "  ./speed-up.awk \"Alien.mp4\" thumb=1 r=15 tempo=1.5\n\n"\
  "  reverse a video with no speed-change, the audio is not reversed. The video\n"\
  "  should be short (<5 Min) as the uncompressed video must fit into memory:\n\n"\
  "  ./speed-up.awk \"talk.mp4\" reversevideo r=1\n\n"\
  "  speed up a video 8 times. Don't run the generated shell script\n"\
  "  (allowing to edit it before running it).\n"\
  "  You can run it manually from the command line with './speedup.sh':\n\n"\
  "  ./speed-up.awk \"myvideo.webm\" r=8 dontrun"
}

function parse_commandline()
{
  for(i=0; i<ARGC; i++)
  {
    option=ARGV[i]

    if( index( option, "r="))
    {
      split( option,op,"r=")
      r=op[2]
    }
    else if( index( option, "thumb="))
    {
      split( option,op,"thumb=")
      thumb_duration=op[2]
    }
    else if( index( option, "tempo="))
    {
      split( option,op,"tempo=")
      tempo=op[2]
    }
    else if( index( option, "pitch="))
    {
      split( option,op,"pitch=")
      pitch=op[2]
    }
    else if( option == "reversevideo")
      reversevideo=1;
    else if( option == "reverseaudio")
      reverseaudio=1;
    else if( option == "dontrun")
      dontrun=1;
    else
      movie_path=option;
  }
}

BEGIN {
  # initialize defaults
  movie_path = ""
  r=10.0
  thumb_duration=1.5
  tempo=1.0
  pitch=1.0
  reversevideo=0
  reverseaudio=0
  dontrun=0

  if( ARGC<=1 )
  {
    print_usage()
    exit
  }

  parse_commandline()

  # get movie duration and audio sample rate
  "ffprobe -of compact -select_streams v -show_entries stream=duration  -select_streams a -show_entries stream=sample_rate \"" movie_path "\" " | getline
  split ( $0, probestring, "duration=")
  split ( probestring[2], probe, "|")
  duration=probe[1]
  split ( $0, probestring, "sample_rate=")
  split ( probestring[2], probe, "|")
  sample_rate=probe[1]
  if (duration=="N/A" || duration=="")
  {
    "ffprobe -of compact -show_entries format=duration \"" movie_path "\" " | getline
    split ( $0, probe, "duration=")
    duration=probe[2]
  }
  if (duration<=0)
  {
    print "Error: No movie."
    exit 1
  }

  # get file extension and base path
  n=split ( movie_path,path_element,".")
  extension=path_element[n]
  movie_base_path=substr(movie_path, 1, length(movie_path)-length(extension)-1)

  # duration of sped-up movie
  new_duration=duration/r

  #print "movie_base_path, extension, sample rate, duration, new_duration: ", movie_base_path, extension, sample_rate, duration, new_duration

  #create shell-script
  script = "#!/bin/sh\n#\n# speedup.sh\n#\n"
  script = script "# run the script from the command line with './speedup.sh'.\n#\n"
  script = script "# the 'ffmpeg'-command at the end of the script can be edited.\n"
  script = script "# example edits:\n"\
                  "# change framerate, worse video quality (default is -crf 23):\n"\
                  "#   ... -filter_complex \"yadif, setpts=PTS/$RATE,framerate=fps=25\" -crf 28 ...\n"\
                  "# motion interpolation (takes longer), higher audio bitrate:\n"\
                  "#   ...  -filter_complex \"yadif, setpts=PTS/$RATE,minterpolate=fps=25\" -ab 256k...\n"\
                  "# resize the video to 640x360 pixels:\n"\
                  "#   ffmpeg -i \"video.webm\" -i \"videox.wav\" -s 640x360 -to $NEWDURATION ...\n"\
                  "# output a .webm instead of the default .mp4:\n"\
                  "#   ... -to $NEWDURATION -filter_complex \"yadif, setpts=PTS/$RATE\" -map 1:0 \"videox.webm\"\n"\
                  "# see also: https://ffmpeg.org/ffmpeg.html\n\#\n\n"

  # assemble pieces of command to speed-up video
  spedup_movie_path=movie_base_path "x.mp4"

  # interpolate frames when the speed-up rate is below 1
  if (r<1)
    frameinterpolate_command=", framerate=fps=25";
  else
    frameinterpolate_command="";

  # reverse
  if (reversevideo>0)
    reverse_command=", reverse";
  else
    reverse_command="";

  #skip when no audio
  if ( sample_rate>0 )
  {

    # split audio from video into temp file
    # optionally change tempo and/or pitch and/or reverse audio
    acommand=""
    if( tempo != 1.0 || pitch != 1.0 || reverseaudio >0)
      acommand="-af \"";
    if( tempo != 1.0)
      acommand=acommand sprintf( "atempo=%f, ", tempo);
    if( pitch != 1.0)
      acommand=acommand sprintf( "asetrate=%d, aresample=%d, ", sample_rate*pitch, sample_rate);
    if (reverseaudio > 0)
      acommand=acommand "areverse";
    if( length (acommand) > 0)
      acommand=acommand "\"";
    sub( ", \"", "\"",acommand);

    command=sprintf( "ffmpeg -i \"%s\" -vn %s \"%s\" ", movie_path, acommand, "speedup-tmp.wav")

    # add command to shell script
    script = script command "\n\n"

    # calculate audio thumbnails
    # duration of tmp audio file
    audio_duration=duration/(tempo*pitch)

    # thumb_distance is the time from the inpoint of a thumbnail to the inpoint
    # of the next one
    thumb_distance=thumb_duration*r/(tempo*pitch)

    # create a textfile for use with ffmpeg -f concat
    # it contains inpoints and outpoints for the audio thumbnails
    thumbsheet = ""

    # start of first thumbnail
    in_point=0.0;

    # add up duration of thumbnails
    sum_thumbnails=0.0;

    while (in_point < audio_duration)
    {
      out_point=in_point+thumb_duration
      if( out_point > audio_duration)
        out_point=audio_duration;
      sum_thumbnails+=(out_point-in_point)
      if( sum_thumbnails>new_duration)
      {
        out_point-=(sum_thumbnails-new_duration);
        if(out_point<=in_point)
          break;
      }
      thumbsheet = thumbsheet "file speedup-tmp.wav\ninpoint " in_point "\noutpoint " out_point "\n"
      in_point+=thumb_distance
    }

    # reverse thumbsheet if necessary
    if( (reversevideo > 0 && reverseaudio <= 0) || (reversevideo<=0 && reverseaudio > 0))
    {
      n_records=split( thumbsheet, thumb_records, "file speedup-tmp.wav\n" )
      thumbsheet=""
      for ( i=n_records; i>0; i--)
      {
         if( thumb_records [i])
           thumbsheet = thumbsheet "file speedup-tmp.wav\n" thumb_records [i];
      }
    }

    # save textfile with in/outpoints to disk
    print thumbsheet > "speedup.txt"

    # add concat-command to script
    audio_path=movie_base_path "x.wav"

    command=sprintf( "ffmpeg -f concat -i speedup.txt \"%s\" ", audio_path)
    script = script command "\n\n"

    # delete tmp files
    command=sprintf( "rm speedup-tmp.wav\n")
    #command=command "mv speedup.txt ~/.Trash/"
    script = script command "\n"

    # Now, unfortunately ffmpeg -f concat isn't accurate:
    # it adds around 15 ms per thumbnail. To compensate, get the real length
    # of the audio-file during runtime of the script and
    # re-calculate the corresponding speed-up rate
    command=sprintf( "MOVIEPATH=\"%s\" \n", movie_path)
    command=command sprintf( "AUDIOPATH=\"%s\" \n", audio_path)
    command=command sprintf("NEWDURATION=`ffprobe -of compact -select_streams a -show_entries stream=duration \"$AUDIOPATH\" | awk 'BEGIN { FS = \"duration=\" } { print $2}'`\n")
    # rate = duration / real_audio_duration
    command=command sprintf( "RATE=`echo \"%s $NEWDURATION\" | awk '{print $1/$2}'`\n", duration)
    command=command sprintf( "echo \"speeding up \\\"$MOVIEPATH\\\" by $RATE. New duration: $NEWDURATION\" seconds.")
    script = script command "\n\n"

    command=sprintf( "ffmpeg -i \"%s\"  -i \"%s\" -to $NEWDURATION -filter_complex \"yadif, setpts=PTS/$RATE%s%s\" -map 1:0 \"%s\"", movie_path, audio_path, frameinterpolate_command, reverse_command, spedup_movie_path)
    script = script command "\n\n"

    # delete file with audio thumbnails
    command=sprintf( "rm \"%s\" ", audio_path)
    #command=sprintf( "mv \"%s\" ~/.Trash/ ", audio_path)
    script = script command "\n"
  } # sample_rate > 0, i.e. there is audio
  else # no audio
  {
    command=sprintf( "ffmpeg -i \"%s\" -filter_complex \"yadif, setpts=PTS/%f%s%s\" \"%s\"", movie_path, r, frameinterpolate_command, reverse_command, spedup_movie_path)
    script = script command "\n"
  }

  # write script to disk and make it executable
  print script > "speedup.sh"
  system ("chmod +x speedup.sh");

  #print script

  # run the script then delete it as well as the textfile with the in/outpoints
  # unless the 'dontrun'-option is set
  if( dontrun>0)
    exit;

  system("sh ./speedup.sh")
  system("rm ./speedup.sh ./speedup.txt")
}




