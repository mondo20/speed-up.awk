speed-up.awk
------------

when you speed up a video by a rate greater than 2 or 3,
the audio, sped up along with the video, gets mangled
and generally becomes unlistenable.

this script by default doesn't speed up the audio, instead
it creates an audio track with stitched together 'audio thumbnails'
giving a good sense of the original audio even if
the video is sped up 10 times or more.

when you use the script to slow down a video, the audio is likewise
not being slowed down, but the 'audio thumbnails' are 'shingled'
(overlapping).

independent of the video, the audio's tempo and pitch can be changed, additionally the video and/or the audio can be reversed.

requirements
------------

this script needs ffmpeg (including ffprobe) to be installed.

examples:
---------

speed up a video by a factor of 10 with audio thumbnails of
1.5 second each:

  `awk -f speed-up.awk video.mp4`

speed up a video 5 times with audio thumbnails of 1 second length:

  `awk -f speed-up.awk video.mp4 r=5 thumb=1`

slow down a video by a factor of 5:

  `awk -f speed-up.awk video.mp4 r=0.2`

speed up a video by a factor of 10 but don't automatically run the
generated shell script:

  `awk -f speed-up.awk video.mp4 dontrun`

the shell script can then be run manually with `./speedup.sh`.
this gives the opportunity to manually edit the ffmpeg-command included at the end ot the shell-script, for example, in order to change the frame size or the output format (by default .mp4).

how it works
------------

it works by generating a shell-script that is then executed.
the shell-script 'speedup.sh' and an additional textfile with
instructions for ffmpeg named 'speedup.txt' are written to
the current working directory and deleted afterwards unless the
script is called with the 'dontrun'-option. The resulting video is written in the same directory as the original video with an x added to the name and a '.mp4'-file extension.
